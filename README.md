# Ethereum block inspector 

It's API helps you to get info about block (count of txs and total value of txs)

### Usage:
* Run a memcache 

```
    docker-compose up -d
```

* Run with Docker container

```
    docker build -f build.dockerfile -t eth_inspector .
    docker run -dp8000:8000 eth_inspector run --config ./config.yaml
```
* Or directly with golang  
```
    go run cmd/eth_inspector --config ./config.yaml
```

