# syntax=docker/dockerfile:1

FROM golang:1.17.2-alpine

WORKDIR $GOPATH/src/gitlab.com/varex83/eth_inspector

COPY . .

RUN go build -o /app ./cmd/eth_inspector
RUN apk add --no-cache ca-certificates

ENTRYPOINT [ "/app" ]