package app

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
	ctxmiddleware "gitlab.com/varex83/eth_inspector/internal/ctx_middleware"
	"gitlab.com/varex83/eth_inspector/internal/handlers"
	midware "gitlab.com/varex83/eth_inspector/internal/middleware"
)

func startRouter(ctx context.Context) error {
	host := ctxhelpers.Config(ctx).App.Host

	ctxhelpers.Logger(ctx).Infof(
		"listening requests on %s",
		host,
	)

	err := http.ListenAndServe(host, router(ctx))
	return err
}

func router(ctx context.Context) *chi.Mux {
	router := chi.NewRouter()

	// adding middleware
	router.Use(
		midware.Ctx(
			ctxmiddleware.EthScanSDK(ctxhelpers.EthScanSDK(ctx)),
			ctxmiddleware.Logger(ctxhelpers.Logger(ctx)),
			ctxmiddleware.Cache(ctxhelpers.Cache(ctx)),
		),
		midware.Logger(ctx),
		midware.ContentType,
		middleware.Recoverer,
	)

	// we're using this contruction insted of /api/block/{block_id}/total
	// because we can easily add new functionality to router without rewriting
	// huge amount of code
	router.Route("/api", func(r chi.Router) {
		r.Route("/block/{block_id}", func(r chi.Router) {
			r.Get("/total", handlers.GetTotalTx)
		})
	})

	return router
}
