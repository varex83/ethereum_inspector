package app

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
	etherscansdk "gitlab.com/varex83/eth_inspector/internal/etherscan_sdk"
)

func Test(t *testing.T) {
	testCases := []struct {
		ID     string
		query  string
		result etherscansdk.BlockModel
	}{
		{
			ID:    "0",
			query: "11509797",
			result: etherscansdk.BlockModel{
				ID: 11509797,
				Data: etherscansdk.BlockModelData{
					Transactions: 155,
					Total:        2.285405,
				},
			},
		},
		{
			ID:    "1",
			query: "11508993",
			result: etherscansdk.BlockModel{
				ID: 11508993,
				Data: etherscansdk.BlockModelData{
					Transactions: 241,
					Total:        1130.987085,
				},
			},
		},
		{
			ID:    "2",
			query: "109789",
			result: etherscansdk.BlockModel{
				ID: 109789,
				Data: etherscansdk.BlockModelData{
					Transactions: 1,
					Total: 4.998770,
				},
			},
		},
	}
	ctx := context.Background()

	ctx = ctxhelpers.AddCache(ctx, memcache.New("localhost:11211"))

	ctx = ctxhelpers.AddLogger(ctx, logrus.New())

	ctx = ctxhelpers.AddEthScanSDK(ctx, etherscansdk.New(ctx, "https://api.etherscan.io", "YourApiKeyToken"))

	server := httptest.NewServer(router(ctx))

	for i, tC := range testCases {
		t.Run(fmt.Sprintf("test %d", i), func(t *testing.T) {
			rawResp, err := http.Get(fmt.Sprintf("%s/api/block/%s/total", server.URL, tC.query))

			assert.NoError(t, err)

			defer rawResp.Body.Close()

			var resp etherscansdk.BlockModel

			assert.NoError(t, json.NewDecoder(rawResp.Body).Decode(&resp))

			assert.Equal(t, resp.ID, tC.result.ID)

			assert.Equal(t, resp.Data.Transactions, tC.result.Data.Transactions)

			assert.LessOrEqual(t, math.Abs(resp.Data.Total-tC.result.Data.Total), 1e-5)

			time.Sleep(time.Second * 1)
		})
	}

}
