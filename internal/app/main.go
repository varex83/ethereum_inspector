package app

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/varex83/eth_inspector/internal/config"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
	etherscansdk "gitlab.com/varex83/eth_inspector/internal/etherscan_sdk"
)

func Run(cfgPath string) error {
	ctx, cancel := context.WithCancel(context.Background())

	shutdownCh := make(chan os.Signal, 1)
	signal.Notify(
		shutdownCh,
		syscall.SIGINT,
		syscall.SIGTERM,
	)

	ctx, err := initCtx(cfgPath, ctx)

	if err != nil {
		cancel()
		return errors.Wrap(err, "error while initializing context: ")
	}

	// worker (1)
	go func() {
		if err := startRouter(ctx); err != nil {
			logrus.WithError(err).Error("error while running router")
			cancel()
		}
	}()

	// listening syscalls or context cancelations from worker (1)
	select {
	case <-shutdownCh:
		break
	case <-ctx.Done():
		break
	}

	ctxhelpers.Logger(ctx).Infoln("gracefully shutting down...")

	cancel()

	ctxhelpers.Logger(ctx).Infoln("bye!")

	return nil
}

// this function initializes context with logger & config
func initCtx(cfgPath string, ctx context.Context) (context.Context, error) {
	ctx = ctxhelpers.AddLogger(ctx, logrus.New())
	logrus.SetLevel(logrus.DebugLevel)

	cfg, err := config.New(cfgPath)

	if err != nil {
		return ctx, errors.Wrap(err, "error while loading config: ")
	}

	ctx = ctxhelpers.AddConfig(ctx, cfg)

	level, err := logrus.ParseLevel(cfg.Log.Level)

	if err != nil {
		// if error in parsing level from config we are using
		// default value [info]
		level = logrus.InfoLevel
	}

	// setting log level
	ctxhelpers.Logger(ctx).SetLevel(level)

	etherscanSdk := etherscansdk.New(ctx, cfg.API.Host, cfg.API.Key)

	if etherscanSdk == nil {
		return ctx, errors.New("error while creating new ethScanSDK instance")
	}

	ctx = ctxhelpers.AddEthScanSDK(ctx, etherscanSdk)

	cache := memcache.New(cfg.Cache.URL)

	if cache == nil {
		return ctx, errors.New("error while connecting to memcache")
	}

	ctx = ctxhelpers.AddCache(ctx, cache)

	return ctx, nil
}
