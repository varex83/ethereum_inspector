package ctxmiddleware

import (
	"context"

	"github.com/sirupsen/logrus"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
)

// adds logger to context (it's context extender)
func Logger(logger *logrus.Logger) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return ctxhelpers.AddLogger(ctx, logger)
	}
}
