package ctxmiddleware

import (
	"context"

	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
	etherscansdk "gitlab.com/varex83/eth_inspector/internal/etherscan_sdk"
)

// adds ethScanSDK to context (it's context extender)
func EthScanSDK(ethclient *etherscansdk.EtherscanSDK) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return ctxhelpers.AddEthScanSDK(ctx, ethclient)
	}
}
