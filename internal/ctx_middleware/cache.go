package ctxmiddleware

import (
	"context"

	"github.com/bradfitz/gomemcache/memcache"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
)

// adds ethScanSDK to context (it's context extender)
func Cache(cache *memcache.Client) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return ctxhelpers.AddCache(ctx, cache)
	}
}
