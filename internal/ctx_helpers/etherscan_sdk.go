package ctxhelpers

import (
	"context"

	etherscansdk "gitlab.com/varex83/eth_inspector/internal/etherscan_sdk"
)

func AddEthScanSDK(ctx context.Context, sdk *etherscansdk.EtherscanSDK) context.Context {
	return context.WithValue(ctx, ethscanSdkCtxKey, sdk)
}

func EthScanSDK(ctx context.Context) *etherscansdk.EtherscanSDK {
	return ctx.Value(ethscanSdkCtxKey).(*etherscansdk.EtherscanSDK)
}
