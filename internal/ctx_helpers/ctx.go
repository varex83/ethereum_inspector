package ctxhelpers

type ctxKey int

const (
	configCtxKey ctxKey = iota
	loggerCtxKey
	ethscanSdkCtxKey
	cacheCtxKey
)
