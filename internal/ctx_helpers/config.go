package ctxhelpers

import (
	"context"

	"gitlab.com/varex83/eth_inspector/internal/config"
)

func AddConfig(ctx context.Context, config *config.Config) context.Context {
	return context.WithValue(ctx, configCtxKey, config)
}

func Config(ctx context.Context) *config.Config {
	return ctx.Value(configCtxKey).(*config.Config)
}
