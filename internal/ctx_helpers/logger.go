package ctxhelpers

import (
	"context"

	"github.com/sirupsen/logrus"
)

func AddLogger(ctx context.Context, logger *logrus.Logger) context.Context {
	return context.WithValue(ctx, loggerCtxKey, logger)
}

func Logger(ctx context.Context) *logrus.Logger {
	return ctx.Value(loggerCtxKey).(*logrus.Logger)
}
