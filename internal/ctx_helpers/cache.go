package ctxhelpers

import (
	"context"

	"github.com/bradfitz/gomemcache/memcache"
)

func AddCache(ctx context.Context, sdk *memcache.Client) context.Context {
	return context.WithValue(ctx, cacheCtxKey, sdk)
}

func Cache(ctx context.Context) *memcache.Client {
	return ctx.Value(cacheCtxKey).(*memcache.Client)
}
