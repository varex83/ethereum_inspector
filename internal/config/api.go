package config

type APIConfig struct {
	Host string `yaml:"url"`
	Key  string `yaml:"key"`
}
