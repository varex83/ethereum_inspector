package config

import (
	"os"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type Config struct {
	App   AppConfig    `yaml:"app"`
	API   APIConfig    `yaml:"api"`
	Log   LoggerConfig `yaml:"log"`
	Cache CacheConfig  `yaml:"memcache"`
}

func New(path string) (*Config, error) {
	defer func() {
		if r := recover(); r != nil {
			logrus.Error("panic when loading config: ")
		}
	}()
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var config *Config
	err = yaml.NewDecoder(f).Decode(&config)
	return config, err
}
