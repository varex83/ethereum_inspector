package validators

import "strconv"

func CheckBlockId(id string) error {
	_, err := strconv.ParseUint(id, 10, 64)

	return err
}
