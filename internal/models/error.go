package models

import (
	"encoding/json"

	"github.com/google/jsonapi"
)

func jsonError(title string, err error) []byte {
	errObj := jsonapi.ErrorObject{
		Title:  title,
		Detail: err.Error(),
	}

	jsonErr, _ := json.Marshal(errObj)

	return jsonErr
}
