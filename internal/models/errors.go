package models

func ErrBadRequest(err error) []byte {
	return jsonError("Bad Request", err)
}

func ErrInternalError(err error) []byte {
	return jsonError("Internal error", err)
}
