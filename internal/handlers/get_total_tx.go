package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/go-chi/chi"
	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
	"gitlab.com/varex83/eth_inspector/internal/models"
	"gitlab.com/varex83/eth_inspector/internal/validators"
)

func GetTotalTx(w http.ResponseWriter, r *http.Request) {
	blockId := chi.URLParam(r, "block_id")

	// check if blockId is dex uint
	if err := validators.CheckBlockId(blockId); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(models.ErrBadRequest(err))

		return
	}

	cache := ctxhelpers.Cache(r.Context())

	// checking if request cached
	if resp, err := cache.Get(blockId); err == nil {
		ctxhelpers.Logger(r.Context()).Debugln("found request in cache")

		w.WriteHeader(http.StatusOK)
		w.Write(resp.Value)

		return
	}

	resp, err := ctxhelpers.EthScanSDK(r.Context()).
		GetBlockByNumber(blockId)

	if err != nil {
		ctxhelpers.Logger(r.Context()).WithError(err).Error("error while getting response from eth_scan")

		w.WriteHeader(http.StatusInternalServerError)
		w.Write(models.ErrInternalError(err))

		return
	}

	// marshaling response
	jsonResp, err := json.Marshal(resp)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(models.ErrInternalError(err))

		ctxhelpers.Logger(r.Context()).WithError(err).Error("error while marshaling response from eth_scan")

		return
	}

	// add request to cache
	err = cache.Set(&memcache.Item{
		Key:   blockId,
		Value: jsonResp,
	})

	if err != nil {
		ctxhelpers.Logger(r.Context()).WithError(err).Error("error while caching request")
	} else {
		ctxhelpers.Logger(r.Context()).Debugln("request cached")
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonResp)
}
