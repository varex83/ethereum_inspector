package middleware

import (
	"context"
	"net/http"

	ctxhelpers "gitlab.com/varex83/eth_inspector/internal/ctx_helpers"
)

// Logging all requests
func Logger(ctx context.Context) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			ctxhelpers.Logger(ctx).Debugf(
				"%s request at %s",
				r.Method, r.RequestURI,
			)
			next.ServeHTTP(rw, r)
		})
	}
}
