package middleware

import (
	"net/http"
)

// Sets content type <application/vnd.api+json> to all responses
func ContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/vnd.api+json")
		next.ServeHTTP(rw, r)
	})
}
