package etherscansdk

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
)

const (
	// requestBlockByNumber pattern
	requestBlockByNumber = "%s/api?module=proxy&action=eth_getBlockByNumber&tag=%s&boolean=true&apikey=%s"
)

type EtherscanSDK struct {
	url    string
	apiKey string
	ctx    context.Context
}

func New(ctx context.Context, url string, apiKey string) *EtherscanSDK {
	return &EtherscanSDK{
		url:    url,
		apiKey: apiKey,
		ctx:    ctx,
	}
}

func (e *EtherscanSDK) GetBlockByNumber(idDexStr string) (BlockModel, error) {
	idDex, err := strconv.ParseInt(idDexStr, 10, 32)

	if err != nil {
		return BlockModel{}, errors.Wrap(err, "error while converting block by number")
	}

	// to hex
	id := fmt.Sprintf("%x", idDex)

	response, err := http.Get(
		fmt.Sprintf(
			requestBlockByNumber,
			e.url, id, e.apiKey,
		),
	)

	if err != nil {
		return BlockModel{}, errors.Wrap(err, "error while getting block info")
	}

	defer response.Body.Close()

	var blockResponse BlockResponse

	err = json.NewDecoder(response.Body).Decode(&blockResponse)

	if err != nil {
		return BlockModel{}, errors.Wrap(err, "error while reading response from ethscan")
	}

	count := blockResponse.getCountOfTxs()
	value, err := blockResponse.getTotalValueOfTxs()

	if err != nil {
		return BlockModel{}, errors.Wrap(err, "error while getting value")
	}

	return BlockModel{
		ID: uint(idDex),
		Data: BlockModelData{
			Transactions: count,
			Total:        value,
		},
	}, nil
}
