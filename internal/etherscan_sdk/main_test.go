package etherscansdk

import (
	"context"
	"fmt"
	"math"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestGetBlockByNumber(t *testing.T) {

	tests := []struct {
		ID           string
		Transactions int64
		Total        float64
	}{
		{"11509797", int64(155), float64(2.285405)},
		{"109789", int64(1), float64(4.99877)},
		{"11508993", int64(241), float64(1130.987085)},
	}

	for i, v := range tests {
		t.Run(fmt.Sprintf("test %d", i), func(tt *testing.T) {
			client := New(context.Background(), "https://api.etherscan.io", "YourApiKeyToken")

			// timeout for etherscan anti-ddos
			time.Sleep(9 * time.Second)

			b, err := client.GetBlockByNumber(v.ID)

			if err != nil {
				tt.Fatal(errors.Wrap(err, "error occurred"))
			}

			assert.Equal(tt, v.Transactions, b.Data.Transactions)

			assert.LessOrEqual(tt, math.Abs(v.Total-b.Data.Total), 1e-5)

		})
	}
}
