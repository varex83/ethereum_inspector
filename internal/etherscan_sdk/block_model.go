package etherscansdk

type BlockModelData struct {
	Transactions int64   `json:"count"`
	Total        float64 `json:"value"`
}

type BlockModel struct {
	ID   uint           `json:"id"`
	Data BlockModelData `json:"data"`
}
