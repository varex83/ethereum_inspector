package etherscansdk

import (
	"math/big"
)

type BlockResponse struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  struct {
		Transactions []struct {
			Value string `json:"value"`
		} `json:"transactions"`
	} `json:"result"`
}

func hexToFloat(hexStr string) float64 {
	i := &big.Int{}
	i.SetString(hexStr, 0)
	f := &big.Float{}

	f.SetInt(i)
	f.Mul(f, big.NewFloat(float64(1e-18)))

	tempVal, _ := f.Float64()

	return tempVal
}

func (br *BlockResponse) getCountOfTxs() int64 {
	return int64(len(br.Result.Transactions))
}

func (br *BlockResponse) getTotalValueOfTxs() (float64, error) {
	var total float64
	for _, valueHex := range br.Result.Transactions {
		valueFloat := hexToFloat(valueHex.Value)
		total += valueFloat
	}
	return total, nil
}
