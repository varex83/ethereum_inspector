# Endpoints 

* #### ```GET /api/block/<block_number>/total```
    * Returns info about block in JSON format 
    * Example
        ``` json
        {
            "id": 1, # block id 
            "data": {
                "transactions": 241, # total transactions
                "total": 1130.987085 # summary transferred (in eth)
            }
        }
        ```