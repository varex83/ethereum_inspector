package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/varex83/eth_inspector/cmd/eth_inspector/commands"
)

func main() {
	cmd := &cobra.Command{
		Use:   "eth_inspector",
		Short: "REST API for ethereum block inspecting",
	}

	cmd.AddCommand(commands.Version())
	cmd.AddCommand(commands.Run())

	logrus.Fatal(cmd.Execute())
}
