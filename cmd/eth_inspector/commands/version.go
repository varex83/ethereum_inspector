package commands

import (
	"fmt"

	"github.com/spf13/cobra"
)

const (
	major  = "0"
	minor  = "1"
	verbal = "beta"
)

func Version() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Show version of eth_inspector",
		Run:   version,
	}
	return cmd
}

func version(cmd *cobra.Command, args []string) {
	fmt.Printf(
		"Version %s.%s-%s",
		major, minor, verbal,
	)
}
