package commands

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/varex83/eth_inspector/internal/app"
)

func Run() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "run",
		Short: "run an eth_inspector",
		RunE:  run,
	}

	cmd.Flags().String(
		"config",
		"config.yaml",
		"configuration file for the eth_inspector",
	)

	return cmd
}

func run(cmd *cobra.Command, args []string) error {
	cfgPath, err := cmd.Flags().GetString("config")

	if err != nil {
		err = errors.Wrap(err, "error while parsing config path: ")
		fmt.Println(err.Error())
		return err
	}

	if err := app.Run(cfgPath); err != nil {
		return errors.Wrap(
			err,
			"error while executing eth_inspector",
		)
	}

	return nil
}
